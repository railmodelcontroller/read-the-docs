# Welcome Rail Model Controller

## What is Rail Model Controller?

**Rail Model Controller**, or **RMC** from now on, is an entire rail model controller made for modelling entusiasts.

**RMC** can control your railway model via _**normal control**_ (such an analogue version) or with a simple _**authomated version**_, maded with _**I2C**_ technology.

## Basics of a railway model controller

A railway model controller must offer to the entusiasts some controls such like:

* speed control
* direction control
* start & stop control
* lights control
* accessories control
    * output types
        * detours
        * cantonments
        * traffic lights
    * input types
        * presence sensors
        * current sensors

and so many other that you can imagine.