# What is Rail Model Controller?

_**Rail Model Controller**_, or just _**RMC**_, is a project to control a model railway model with some new technologies.

The principal idea of this project is offer to the end user the way to contol their railway model with its own smartphone, tablet or pc, for example.

[![Overview](img/overview/overview.png)](img/overview/overview.png)

## Inputs

TODO

## Controller Core

TODO

## Outputs

TODO


---

Please see the [project license](license.md) for further details.
